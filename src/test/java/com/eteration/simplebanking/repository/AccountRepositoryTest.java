package com.eteration.simplebanking.repository;

import com.eteration.simplebanking.model.Account;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import javax.transaction.Transactional;
import java.math.BigDecimal;

import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest
class AccountRepositoryTest {

    @Autowired
    AccountRepository accountRepository;

    @Test
    public void should_create_account() {
        var account = new Account("Oktay", "1234");
        accountRepository.save(account);

        var found = accountRepository.findById(account.getId());
        assertThat(found).isPresent();
        assertThat(found).hasValueSatisfying(a -> {
            assertThat(a.getAccountNumber()).isEqualTo(account.getAccountNumber());
        });
    }

    @Test
    @Transactional
    public void should_deposit() {
        var account = new Account("Oktay", "1234");
        accountRepository.save(account);

        account.deposit(new BigDecimal("100"));

        accountRepository.save(account);

        var updated = accountRepository.findById(account.getId());

        assertThat(updated).hasValueSatisfying(a -> {
            assertThat(a.getTransactions()).isNotEmpty();
            assertThat(a.getBalance()).isEqualTo(new BigDecimal("100"));
        });

    }
}