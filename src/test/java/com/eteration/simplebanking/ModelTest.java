package com.eteration.simplebanking;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import com.eteration.simplebanking.model.Account;
import com.eteration.simplebanking.model.DepositTransaction;
import com.eteration.simplebanking.model.InsufficientBalanceException;
import com.eteration.simplebanking.model.WithdrawalTransaction;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.Objects;

public class ModelTest {

    @Test
    public void testCreateAccountAndSetBalance0() {
        Account account = new Account("Kerem Karaca", "17892");
        assertEquals("Kerem Karaca", account.getOwner());
        assertEquals("17892", account.getAccountNumber());
        assertTrue(account.getBalance().equals(bd("0")));
    }

    @Test
    public void testDepositIntoBankAccount() {
        Account account = new Account("Demet Demircan", "9834");
        account.deposit(bd("100"));
        assertTrue(Objects.equals(account.getBalance(), bd("100")));
    }

    @Test
    public void testWithdrawFromBankAccount() throws InsufficientBalanceException {
        Account account = new Account("Demet Demircan", "9834");
        var amount = bd("100");
        account.deposit(amount);
        assertEquals(account.getBalance(), amount);
        account.withdraw(bd("50"));
        assertEquals(account.getBalance(), bd("50"));
    }


    @Test
    public void testWithdrawException() {
        Assertions.assertThrows(InsufficientBalanceException.class, () -> {
            Account account = new Account("Demet Demircan", "9834");
            account.deposit(bd("100"));
            account.withdraw(bd("500"));
        });

    }

    @Test
    public void testTransactions() throws InsufficientBalanceException {
        // Create account
        Account account = new Account("Canan Kaya", "1234");
        assertTrue(account.getTransactions().size() == 0);

        // Deposit Transaction
        DepositTransaction depositTrx = new DepositTransaction(bd("100"));

        assertTrue(depositTrx.getDate() != null);
        account.post(depositTrx);
        assertTrue(account.getBalance().equals(bd("100")));
        assertTrue(account.getTransactions().size() == 1);

        // Withdrawal Transaction
        WithdrawalTransaction withdrawalTrx = new WithdrawalTransaction(bd("60"));
        assertTrue(withdrawalTrx.getDate() != null);
        account.post(withdrawalTrx);
        assertTrue(Objects.equals(account.getBalance(), bd("40")));
        assertTrue(account.getTransactions().size() == 2);
    }

    static BigDecimal bd(String value) {
        return new BigDecimal(value);
    }
}
