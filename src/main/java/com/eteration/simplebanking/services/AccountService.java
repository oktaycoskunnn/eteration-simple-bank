package com.eteration.simplebanking.services;


import com.eteration.simplebanking.model.Account;
import com.eteration.simplebanking.model.InsufficientBalanceException;
import com.eteration.simplebanking.repository.AccountRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class AccountService {

    private final AccountRepository accountRepository;

    public Account findAccount(String accountNumber) {
        return getAccount(accountNumber);

    }

    @Transactional
    public UUID credit(String accountNumber, BigDecimal amount) {
        var account = getAccount(accountNumber);
        var approvalCode = account.deposit(amount);
        accountRepository.save(account);
        return approvalCode;
    }

    @Transactional
    public UUID debit(String accountNumber, BigDecimal amount) throws InsufficientBalanceException {
        var account = getAccount(accountNumber);
        var approvalCode = account.withdraw(amount);
        accountRepository.save(account);
        return approvalCode;
    }

    private Account getAccount(String accountNumber) {
        return accountRepository.findByAccountNumber(accountNumber)
                .orElseThrow(() -> new NullPointerException("account not found"));
    }
}
