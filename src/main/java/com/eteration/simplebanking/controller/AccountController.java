package com.eteration.simplebanking.controller;

import com.eteration.simplebanking.model.*;
import com.eteration.simplebanking.services.AccountService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequiredArgsConstructor
@RequestMapping("/account/v1")
public class AccountController {
    private final AccountService accountService;

    @GetMapping("/{accountNumber}")
    public ResponseEntity<Account> getAccount(@PathVariable String accountNumber) {
        return ResponseEntity.of(Optional.of(accountService.findAccount(accountNumber)));

    }

    @PostMapping("/credit/{accountNumber}")
    public ResponseEntity<TransactionStatus> credit(@PathVariable String accountNumber, @RequestBody DepositTransaction depositTransaction) {
        var approvalCode = accountService.credit(accountNumber, depositTransaction.getAmount());
        return ResponseEntity.of(Optional.of(new TransactionStatus(HttpStatus.OK.getReasonPhrase(), approvalCode)));
    }

    @PostMapping("/debit/{accountNumber}")
    public ResponseEntity<TransactionStatus> debit(@PathVariable String accountNumber, @RequestBody WithdrawalTransaction withdrawalTransaction) throws InsufficientBalanceException {
        var approvalCode = accountService.debit(accountNumber, withdrawalTransaction.getAmount());
        return ResponseEntity.of(Optional.of(new TransactionStatus(HttpStatus.OK.getReasonPhrase(), approvalCode)));
    }
}