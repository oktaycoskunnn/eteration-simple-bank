package com.eteration.simplebanking.model;

import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Entity
@Getter
@NoArgsConstructor
public class Account {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;
    String owner;
    String accountNumber;
    BigDecimal balance;
    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "accountId")
    List<Transaction> transactions = new ArrayList<>();

    public Account(String name, String accountNumber) {
        this.owner = name;
        this.accountNumber = accountNumber;
        this.balance = BigDecimal.ZERO;
    }

    public UUID deposit(BigDecimal price) {
        DepositTransaction transaction = new DepositTransaction(price);
        post(transaction);
        return transaction.getApprovalCode();
    }

    public UUID withdraw(BigDecimal price) throws InsufficientBalanceException {
        if (price.compareTo(balance) > 0)
            throw new InsufficientBalanceException();
        WithdrawalTransaction transaction = new WithdrawalTransaction(price);
        post(transaction);
        return transaction.getApprovalCode();
    }

    public void post(DepositTransaction depositTrx) {
        balance = balance.add(depositTrx.getAmount());
        this.transactions.add(depositTrx);
    }

    public void post(WithdrawalTransaction withdrawalTransaction) {
        balance = balance.subtract(withdrawalTransaction.getAmount());
        this.transactions.add(withdrawalTransaction);
    }
}