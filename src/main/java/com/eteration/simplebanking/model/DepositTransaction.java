package com.eteration.simplebanking.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.math.BigDecimal;

@Entity
@Getter
@NoArgsConstructor
@DiscriminatorValue("DepositTransaction")
public class DepositTransaction extends Transaction {
    public DepositTransaction(BigDecimal amount) {
        create(amount);
    }
}
