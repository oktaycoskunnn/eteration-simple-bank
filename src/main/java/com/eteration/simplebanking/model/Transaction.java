package com.eteration.simplebanking.model;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.OffsetDateTime;
import java.util.UUID;

@Entity
@ToString
@Getter
@NoArgsConstructor
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "type", discriminatorType = DiscriminatorType.STRING)
public abstract class Transaction {
    @Id
    private UUID approvalCode;
    OffsetDateTime date;
    BigDecimal amount;

    protected void create(BigDecimal amount) {
        this.approvalCode = UUID.randomUUID();
        this.date = OffsetDateTime.now();
        this.amount = amount;
    }

    @Transient
    public String getType(){
        if (this instanceof DepositTransaction)
            return "DepositTransaction";
        if (this instanceof WithdrawalTransaction)
            return "WithdrawalTransaction";
        throw new IllegalArgumentException("unknown transaction type!");
    }
}
