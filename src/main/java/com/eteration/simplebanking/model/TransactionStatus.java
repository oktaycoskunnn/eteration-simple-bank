package com.eteration.simplebanking.model;


import lombok.Value;

import java.util.UUID;

@Value
public class TransactionStatus {
    String status;
    UUID approvalCode;

}
